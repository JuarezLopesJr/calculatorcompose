package com.example.calculatorcomposem3.presentation

data class CalculatorState(
    val value1: String = "",
    val value2: String = "",
    val operation: CalculatorOperation? = null
)
