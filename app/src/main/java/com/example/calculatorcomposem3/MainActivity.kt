package com.example.calculatorcomposem3

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.calculatorcomposem3.presentation.Calculator
import com.example.calculatorcomposem3.ui.theme.CalculatorComposeM3Theme
import com.example.calculatorcomposem3.viewmodel.CalculatorViewModel

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            CalculatorComposeM3Theme {
                val viewModel = viewModel<CalculatorViewModel>()
                val state = viewModel.state

                Calculator(state = state, viewModel = viewModel)
            }
        }
    }
}