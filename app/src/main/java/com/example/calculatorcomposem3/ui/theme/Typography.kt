package com.example.calculatorcomposem3.ui.theme

import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import com.example.calculatorcomposem3.R

val Adamina = FontFamily(
    Font(R.font.adamina)
)