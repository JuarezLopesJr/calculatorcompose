package com.example.calculatorcomposem3.viewmodel

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import com.example.calculatorcomposem3.presentation.CalculatorAction
import com.example.calculatorcomposem3.presentation.CalculatorOperation
import com.example.calculatorcomposem3.presentation.CalculatorState

class CalculatorViewModel : ViewModel() {

    companion object {
        private const val MAX_NUM_LENGTH = 8
    }

    var state by mutableStateOf(CalculatorState())
        private set

    fun onAction(action: CalculatorAction) {
        when (action) {
            is CalculatorAction.Number -> enterNumber(action.number)
            is CalculatorAction.Delete -> delete()
            is CalculatorAction.Clear -> state = CalculatorState()
            is CalculatorAction.Operation -> enterOperation(action.operation)
            is CalculatorAction.Decimal -> enterDecimal()
            is CalculatorAction.Calculate -> calculate()
        }
    }

    private fun enterOperation(operation: CalculatorOperation) {
        if (state.value1.isNotBlank()) {
            state = state.copy(operation = operation)
        }
    }

    private fun calculate() {
        val value1 = state.value1.toDoubleOrNull()
        val value2 = state.value2.toDoubleOrNull()
        if (value1 != null && value2 != null) {
            val result = when (state.operation) {
                is CalculatorOperation.Add -> value1 + value2
                is CalculatorOperation.Subtract -> value1 - value2
                is CalculatorOperation.Multiply -> value1 * value2
                is CalculatorOperation.Divide -> value1 / value2
                null -> return
            }
            state = state.copy(
                value1 = result.toString().take(15),
                value2 = "",
                operation = null
            )
        }
    }

    private fun delete() {
        when {
            state.value2.isNotBlank() -> state = state.copy(
                value2 = state.value2.dropLast(1)
            )
            state.operation != null -> state = state.copy(
                operation = null
            )
            state.value1.isNotBlank() -> state = state.copy(
                value1 = state.value1.dropLast(1)
            )
        }
    }

    private fun enterDecimal() {
        if (state.operation == null && !state.value1.contains(".") && state.value1.isNotBlank()) {
            state = state.copy(
                value1 = state.value1 + "."
            )
            return
        } else if (!state.value2.contains(".") && state.value2.isNotBlank()) {
            state = state.copy(
                value2 = state.value2 + "."
            )
        }
    }

    private fun enterNumber(number: Int) {
        if (state.operation == null) {
            if (state.value1.length >= MAX_NUM_LENGTH) {
                return
            }
            state = state.copy(
                value1 = state.value1 + number
            )
            return
        }
        if (state.value2.length >= MAX_NUM_LENGTH) {
            return
        }
        state = state.copy(
            value2 = state.value2 + number
        )
    }
}